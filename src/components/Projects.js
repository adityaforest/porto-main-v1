import { Container, Row, Col, Tab, Nav } from "react-bootstrap";
import { ProjectCard } from "./ProjectCard";
import projImg1 from "../assets/img/project-img1.png";
import projImg2 from "../assets/img/project-img2.png";
import projImg3 from "../assets/img/project-img3.png";
import projImg4 from "../assets/img/project-img4.png";
import projImg5 from "../assets/img/project-img5.png"
import colorSharp2 from "../assets/img/color-sharp2.png";
import 'animate.css';
import TrackVisibility from 'react-on-screen';

export const Projects = () => {

  const projects = [
    {
      title: "This Portofolio",
      description: "React , Bootstrap",
      imgUrl: projImg1,
      gitUrl: ["https://gitlab.com/adityaforest/porto-main-v1"],
      siteUrl: ""
    },
    {
      title: "Forest Chat App",
      description: "NextJS , Tailwind , Firebase RTDB",
      imgUrl: projImg2,
      gitUrl: ["https://gitlab.com/adityaforest/chatapp-firebase"],
      siteUrl: "https://forestchat.vercel.app/"
    },
    {
      title: "PlayerOne Game Website (NextJS)",
      description: "SCRUM , NextJS , Redux , Bootstrap , Firebase RTDB , NodeJS (Express) , JWT , PostgreSQL , Sequelize",
      imgUrl: projImg3,
      gitUrl: ["https://gitlab.com/fsw27/playerone","https://gitlab.com/fsw27/playerone-be"],
      siteUrl: "https://playerone-pearl.vercel.app/"
    },
    {
      title: "PlayerOne Game Website (ReactJS)",
      description: "SCRUM , TDD Jest , ReactJS , Bootstrap , Cloudinary , Firebase RTDB , NodeJS (Express) , JWT , PostgreSQL , Sequelize",
      imgUrl: projImg4,
      gitUrl: ["https://gitlab.com/fsw27/playerone-fe","https://gitlab.com/fsw27/playerone-be"],
      siteUrl: "https://playerone-fe.vercel.app/ "
    },{
      title: 'To Do App Sample',
      description:'React , Redux , Bootstrap , NodeJS(Express) , Sequelize , PostgreSQL',
      imgUrl: projImg5,
      gitUrl: ["https://gitlab.com/adityaforest/todoapp-be","https://gitlab.com/adityaforest/todoapp-fe"],
      siteUrl: "https://todoapp-fe.vercel.app/"
    }
  ];

  return (
    <section className="project" id="projects">
      <Container>
        <Row>
          <Col size={12}>
            <TrackVisibility>
              {({ isVisible }) =>
                <div className={isVisible ? "animate__animated animate__fadeIn" : ""}>
                  <h2>Projects</h2>
                  <p>Below are some of the projects I've worked on.</p>
                  <Tab.Container id="projects-tabs" defaultActiveKey="first">
                    <Nav variant="pills" className="nav-pills mb-5 justify-content-center align-items-center" id="pills-tab">
                      <Nav.Item>
                        <Nav.Link eventKey="first">Personal Showcase</Nav.Link>
                      </Nav.Item>
                      <Nav.Item>
                        <Nav.Link eventKey="second">Real Projects</Nav.Link>
                      </Nav.Item>
                      <Nav.Item>
                        <Nav.Link eventKey="third">Others</Nav.Link>
                      </Nav.Item>
                    </Nav>
                    <Tab.Content id="slideInUp" className={isVisible ? "animate__animated animate__slideInUp" : ""}>
                      <Tab.Pane eventKey="first">
                        <Row>
                          {
                            projects.map((project, index) => {
                              return (
                                <ProjectCard
                                  key={index}
                                  {...project}
                                />
                              )
                            })
                          }
                        </Row>
                      </Tab.Pane>
                      <Tab.Pane eventKey="second">
                        <p>There isn't any yet :(</p>
                      </Tab.Pane>
                      <Tab.Pane eventKey="third">
                        <p>There isn't any yet :(</p>
                      </Tab.Pane>
                    </Tab.Content>
                  </Tab.Container>
                </div>}
            </TrackVisibility>
          </Col>
        </Row>
      </Container>
      <img className="background-image-right" src={colorSharp2}></img>
    </section>
  )
}
