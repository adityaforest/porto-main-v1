// import { useState, useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import headerImg from "../assets/img/header-img.png";
import { ArrowRightCircle } from 'react-bootstrap-icons';
import 'animate.css';
import TrackVisibility from 'react-on-screen';

export const Banner = () => {
  return (
    <section className="banner" id="home">
      <div className="overlay">
        <Container>
          <Row className="aligh-items-center">
            <Col xs={12} md={6} xl={7}>
              <TrackVisibility>
                {({ isVisible }) =>
                  <div className={isVisible ? "animate__animated animate__fadeIn" : ""}>
                    <span className="tagline">Welcome to my Portfolio</span>
                    <h1>
                      Hi! I'm Forest,
                      Full Stack Developer
                    </h1>
                    <p>                      
                      I'm a fast learner and i'm addicted to everything that includes logic in it.
                      Let's create some magic through programming !
                    </p>
                    <a href="mailto:adityaforestresananta@gmail.com" target="_blank" style={{ textDecoration:"none" }}>
                    <button>
                      Let’s Connect <ArrowRightCircle size={25} />
                    </button>
                    </a>
                  </div>}
              </TrackVisibility>
            </Col>
            <Col xs={12} md={6} xl={5}>
              <TrackVisibility>
                {({ isVisible }) =>
                  <div className={isVisible ? "animate__animated animate__zoomIn" : ""}>
                    <img src={headerImg} alt="Header Img" />
                  </div>}
              </TrackVisibility>
            </Col>
          </Row>
        </Container>
      </div>
    </section>
  )
}
