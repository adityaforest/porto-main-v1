import meter1 from "../assets/img/meter1.svg";
import meter2 from "../assets/img/meter2.svg";
import meter3 from "../assets/img/meter3.svg";
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import arrow1 from "../assets/img/arrow1.svg";
import arrow2 from "../assets/img/arrow2.svg";
import colorSharp from "../assets/img/color-sharp.png"

export const Skills = () => {
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1
    }
  };

  return (
    <section className="skill" id="skills">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="skill-bx wow zoomIn">
              <h2>Skills</h2>
              <p>These are some of the skills or stacks that I've already learned.</p>
              <Carousel responsive={responsive} infinite={true} className="owl-carousel owl-theme skill-slider">
                <div className="item">
                  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/61/HTML5_logo_and_wordmark.svg/640px-HTML5_logo_and_wordmark.svg.png"
                  />
                  <h5>HTML</h5>
                </div>
                <div className="item">
                  <img src="https://cdn.pixabay.com/photo/2017/08/05/11/16/logo-2582747_1280.png"
                  />
                  <h5>CSS</h5>
                </div>
                <div className="item">
                  <img src="https://icon-library.com/images/javascript-icon-png/javascript-icon-png-23.jpg" alt="" />
                  <h5>JS</h5>
                </div>
                <div className="item">
                  <img src="https://static.vecteezy.com/system/resources/previews/012/697/297/original/3d-bootstrap-programming-framework-logo-free-png.png" alt="" />
                  <h5>Bootstrap</h5>
                </div>
                <div className="item">
                  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Tailwind_CSS_Logo.svg/1024px-Tailwind_CSS_Logo.svg.png" alt="" />
                  <h5>Tailwind</h5>
                </div>
                <div className="item">
                  <img src="https://mui.com/static/logo.png" alt="" />
                  <h5>Material UI</h5>
                </div>
                <div className="item">
                  <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSt9ew0h7w66Rer66EasIYeo23tncThSQmvHlBhK-yRcazVpX-o6iIFgiHySAz60kOU0DA&usqp=CAU" alt="" />
                  <h5>EJS</h5>
                </div>
                <div className="item">
                  <img src="https://miro.medium.com/max/512/1*zXu2vsYPZ5mqF0tOB7kupA.png" 
                  alt=""                   
                  />
                  <h5>ReactJS</h5>
                </div>
                <div className="item">
                  <img src="https://repository-images.githubusercontent.com/347723622/92065800-865a-11eb-9626-dff3cb7fef55" 
                  alt=""                   
                  />
                  <h5>Redux</h5>
                </div>
                <div className="item">
                  <img src="https://miro.medium.com/max/512/1*zXu2vsYPZ5mqF0tOB7kupA.png" 
                  alt=""                   
                  />
                  <h5>React Native</h5>
                </div>
                <div className="item">
                  <img src="https://www.rlogical.com/wp-content/uploads/2021/08/Rlogical-Blog-Images-thumbnail.png" alt="" />
                  <h5>NextJS</h5>
                </div>
                <div className="item">
                  <img src="https://walde.co/wp-content/uploads/2016/09/nodejs_logo.png" alt="" />
                  <h5>NodeJS (Express)</h5>
                </div>
                <div className="item">
                  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Laravel.svg/985px-Laravel.svg.png" 
                  alt=""                   
                  />
                  <h5>PHP (Laravel)</h5>
                </div>
                <div className="item">
                  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Postgresql_elephant.svg/1985px-Postgresql_elephant.svg.png" 
                  alt=""                   
                  />
                  <h5>PostgreSQL</h5>
                </div>
                <div className="item">
                  <img src="https://www.freepnglogos.com/uploads/logo-mysql-png/logo-mysql-mysql-logo-png-images-are-download-crazypng-21.png" 
                  alt=""                   
                  />
                  <h5>MySQL</h5>
                </div>
                <div className="item">
                  <img src="https://www.pngall.com/wp-content/uploads/13/Mongodb-PNG-Photo.png" 
                  alt=""                   
                  />
                  <h5>MongoDB</h5>
                </div>
                <div className="item">
                  <img src="https://cdn.iconscout.com/icon/free/png-256/sequelize-1175001.png" 
                  alt=""                   
                  />
                  <h5>Sequelize ORM</h5>
                </div>
                <div className="item">
                  <img src="https://cdn.cdnlogo.com/logos/j/20/jwt.svg" 
                  alt=""                   
                  />
                  <h5>JWT</h5>
                </div>
                <div className="item">
                  <img src="https://play-lh.googleusercontent.com/CxmsLct-ExxgB8p-qyV5897AtVUL9UqKS1IQJ8AF88AMzXSQ1RMIVwtvuQfnwyxE3bIh" 
                  alt=""                   
                  />
                  <h5>Websocket</h5>
                </div>
                <div className="item">
                  <img src="https://upload.wikimedia.org/wikipedia/commons/a/ab/Swagger-logo.png" 
                  alt=""                   
                  />
                  <h5>Swagger</h5>
                </div>
                <div className="item">
                  <img src="https://cdn.worldvectorlogo.com/logos/postman.svg" 
                  alt=""                   
                  />
                  <h5>Postman</h5>
                </div>
                <div className="item">
                  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Gitlab_meaningful_logo.svg/1024px-Gitlab_meaningful_logo.svg.png" 
                  alt=""                   
                  />
                  <h5>Git / GitLab</h5>
                </div>
              </Carousel>
            </div>
          </div>
        </div>
      </div>
      <img className="background-image-left" src={colorSharp} alt="Image" />
    </section>
  )
}
