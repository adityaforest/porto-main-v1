import { Col } from "react-bootstrap";
import navIcon1 from '../assets/img/nav-icon1.svg';
import navIcon2 from '../assets/img/nav-icon2.svg';
import navIcon3 from '../assets/img/nav-icon3.svg';

export const ProjectCard = ({ title, description, imgUrl, gitUrl, siteUrl }) => {
  return (
    <Col size={12} sm={6} md={4}>
      <div className="proj-imgbx">
        <img src={imgUrl} style={{ width: "518px", height: "300px" }} />
        <div className="proj-txtx">
          <div className="row justify-content-center">
            <div className="col-md-12">
              <h4>{title}</h4>
              <span>{description}</span>
            </div>
            <div className="social-icon" style={{ marginTop: "30px", marginBottom: "20px" }}>
              <a href={gitUrl[0]} target="_blank"><img src={"https://seeklogo.com/images/G/gitlab-logo-757620E430-seeklogo.com.png"} alt="" /></a>
              {
                (gitUrl.length == 2)
                  ?
                  <a href={gitUrl[1]} target="_blank"><img src={"https://seeklogo.com/images/G/gitlab-logo-757620E430-seeklogo.com.png"} alt="" /></a>
                  :
                  null
              }
            </div>


            <a className="btn btn-light" href={siteUrl} target="_blank">Visit Site</a>

          </div>
        </div>
      </div>
    </Col>
  )
}
